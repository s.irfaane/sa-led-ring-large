<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCopper" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCopper" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="Anatomie" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Brücken" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="CableCode" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="tdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="bdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="L$199" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="Extra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="Cool" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="bExtra" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N" xrefpart="/%S.%C%R">
<libraries>
<library name="Eagle_WIRE-TO-BOARD_rev16a">
<description>&lt;BR&gt;Wurth Elektronik - Wire to Board Connectors&lt;br&gt;&lt;Hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-405&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/eagle"&gt;http://www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;
&lt;hr&gt;
Eagle Version 6, Library Revision 2015c, 2015-06-02&lt;br&gt;
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="62000311722">
<description>WR-WTB 2.00mm Male Horizontal Shrouded Header, 4 Pins</description>
<wire x1="-5.125" y1="1.35" x2="3.125" y2="1.35" width="0.127" layer="51"/>
<wire x1="3.125" y1="1.35" x2="3.125" y2="-6.25" width="0.127" layer="51"/>
<wire x1="3.125" y1="-6.25" x2="-5.125" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-5.125" y1="-6.25" x2="-5.125" y2="1.35" width="0.127" layer="51"/>
<wire x1="-5.125" y1="1.35" x2="3.125" y2="1.35" width="0.127" layer="21"/>
<wire x1="3.125" y1="1.35" x2="3.125" y2="-6.25" width="0.127" layer="21"/>
<wire x1="3.125" y1="-6.25" x2="-5.125" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-5.125" y1="-6.25" x2="-5.125" y2="1.35" width="0.127" layer="21"/>
<pad name="1" x="1" y="0" drill="0.8"/>
<pad name="2" x="-1" y="0" drill="0.8"/>
<pad name="3" x="-3" y="0" drill="0.8"/>
<text x="3.785" y="-0.05" size="1.27" layer="25">&gt;NAME</text>
<text x="4.01" y="-2.03" size="1.27" layer="27">&gt;VALUE</text>
<text x="0.5" y="-0.5" size="1.27" layer="51">1</text>
<text x="-3.5" y="-0.5" size="1.27" layer="51">3</text>
</package>
</packages>
<symbols>
<symbol name="3">
<wire x1="-10.16" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<text x="7.62" y="0" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="2.54" y="-7.62" length="middle" direction="pas" rot="R90"/>
<pin name="2" x="-2.54" y="-7.62" length="middle" direction="pas" rot="R90"/>
<pin name="3" x="-7.62" y="-7.62" length="middle" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="62000311722" prefix="J" uservalue="yes">
<description>&lt;b&gt; WR-WTB 2.00mm Male Horizontal Shrouded Header, 3 Pins&lt;/b&gt;=&gt;Code : Con_WTB_2.00_PCB_THT_62000311722
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/thumbs2/eican/thb_Con_WTB_2.00_PCB_THT_6200xx11722_pf2.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/thumbs2/eican/thb_Con_WTB_2.00_PCB_THT_6200xx11722_pf2.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/en/em/620_0xx_117_22"&gt;http://katalog.we-online.de/en/em/620_0xx_117_22&lt;/a&gt;&lt;p&gt;
Created 2014-06-05, Karrer Zheng&lt;br&gt;
2014 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="62000311722">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OSPIN">
<packages>
<package name="WS2812B">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<wire x1="-1.6" y1="2.5" x2="-1.3" y2="2.8" width="0.127" layer="21"/>
<wire x1="-1.3" y1="2.8" x2="-1.7" y2="3.2" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.2" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="WS2812B-NARROW">
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-1.6" y2="2.5" width="0.127" layer="21"/>
<smd name="1-VDD" x="2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="2-DOUT" x="2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="4-DIN" x="-2.35" y="-1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<smd name="3-GND" x="-2.35" y="1.65" dx="1.3" dy="1.2" layer="1" rot="R180"/>
<circle x="0" y="0" radius="1.7204625" width="0.127" layer="21"/>
<text x="3.4925" y="1.5875" size="0.8128" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<wire x1="-1.6" y1="2.5" x2="-1.25" y2="2.85" width="0.127" layer="21"/>
<wire x1="-1.25" y1="2.85" x2="-1.7" y2="3.3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3.3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="LED3535">
<smd name="1" x="-1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="4" x="1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.4" width="0.127" layer="51"/>
<wire x1="-1.9" y1="1.6" x2="-1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.9" x2="1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.9" x2="1.9" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.6" x2="-1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.9" x2="1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.9" x2="1.9" y2="-1.6" width="0.127" layer="21"/>
<text x="-1.905" y="2.159" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-2.54" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-1.905" y="1.905"/>
<vertex x="-1.905" y="1.524"/>
<vertex x="-1.524" y="1.524"/>
<vertex x="-1.143" y="1.905"/>
</polygon>
</package>
<package name="0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.973" y1="0.483" x2="0.973" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.973" y1="0.483" x2="0.973" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.973" y1="-0.483" x2="-0.973" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.973" y1="-0.483" x2="-0.973" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.45" y="0" dx="0.4" dy="0.5" layer="1"/>
<smd name="2" x="0.45" y="0" dx="0.4" dy="0.5" layer="1"/>
<text x="-0.635" y="0.635" size="0.762" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.27" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.223" y1="0.608" x2="1.223" y2="0.608" width="0.0508" layer="39"/>
<wire x1="1.223" y1="0.608" x2="1.223" y2="-0.608" width="0.0508" layer="39"/>
<wire x1="1.223" y1="-0.608" x2="-1.223" y2="-0.608" width="0.0508" layer="39"/>
<wire x1="-1.223" y1="-0.608" x2="-1.223" y2="0.608" width="0.0508" layer="39"/>
<smd name="1" x="-0.75" y="0" dx="0.5" dy="0.8" layer="1"/>
<smd name="2" x="0.75" y="0" dx="0.5" dy="0.8" layer="1"/>
<text x="-0.635" y="0.635" size="0.762" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.5875" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.598" y1="0.858" x2="1.598" y2="0.858" width="0.0508" layer="39"/>
<wire x1="1.598" y1="0.858" x2="1.598" y2="-0.858" width="0.0508" layer="39"/>
<wire x1="1.598" y1="-0.858" x2="-1.598" y2="-0.858" width="0.0508" layer="39"/>
<wire x1="-1.598" y1="-0.858" x2="-1.598" y2="0.858" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.15" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.15" layer="1"/>
<text x="-0.635" y="1.27" size="0.762" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.223" y1="0.983" x2="2.223" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.223" y1="0.983" x2="2.223" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.223" y1="-0.983" x2="-2.223" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.223" y1="-0.983" x2="-2.223" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.55" y="0" dx="0.9" dy="1.5" layer="1"/>
<smd name="1" x="-1.55" y="0" dx="0.9" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="0.762" layer="25">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.223" y1="1.483" x2="2.223" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.223" y1="1.483" x2="2.223" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.223" y1="-1.483" x2="-2.223" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.223" y1="-1.483" x2="-2.223" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.55" y="0" dx="0.9" dy="2.3" layer="1"/>
<smd name="2" x="1.55" y="0" dx="0.9" dy="2.3" layer="1"/>
<text x="-2.2225" y="1.5875" size="0.762" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.223" y1="1.483" x2="3.223" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.223" y1="1.483" x2="3.223" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.223" y1="-1.483" x2="-3.223" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.223" y1="-1.483" x2="-3.223" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.45" y="0" dx="1.1" dy="2.3" layer="1"/>
<smd name="2" x="2.45" y="0" dx="1.1" dy="2.3" layer="1"/>
<text x="-3.175" y="1.655" size="0.762" layer="25">&gt;NAME</text>
<text x="-3.175" y="-2.425" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-4.223" y1="1.983" x2="4.223" y2="1.983" width="0.0508" layer="39"/>
<wire x1="4.223" y1="1.983" x2="4.223" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="4.223" y1="-1.983" x2="-4.223" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-4.223" y1="-1.983" x2="-4.223" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.3" y="0" dx="1.5" dy="3" layer="1"/>
<smd name="2" x="3.3" y="0" dx="1.5" dy="3" layer="1"/>
<text x="-2.54" y="1.905" size="0.762" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.925" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="WS2812BLED">
<pin name="VDD" x="5.08" y="15.24" visible="pin" length="middle" direction="pwr" rot="R270"/>
<pin name="DI" x="-12.7" y="-2.54" visible="pin" length="middle" direction="in"/>
<pin name="GND" x="0" y="-10.16" visible="pin" length="middle" direction="pwr" rot="R90"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="middle" direction="out" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-4.064" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="-4.064" y="8.382" size="1.27" layer="94">WS2812B</text>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WS2812B" prefix="LED">
<gates>
<gate name="G$1" symbol="WS2812BLED" x="0" y="-2.54"/>
</gates>
<devices>
<device name="5050" package="WS2812B">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5050N" package="WS2812B-NARROW">
<connects>
<connect gate="G$1" pin="DI" pad="4-DIN"/>
<connect gate="G$1" pin="DO" pad="2-DOUT"/>
<connect gate="G$1" pin="GND" pad="3-GND"/>
<connect gate="G$1" pin="VDD" pad="1-VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3535" package="LED3535">
<connects>
<connect gate="G$1" pin="DI" pad="1"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="Eagle_WIRE-TO-BOARD_rev16a" deviceset="62000311722" device="">
<attribute name="WE_BEST_NR" value="68800211622"/>
</part>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="LED1" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C1" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="LED2" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C2" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="LED3" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C3" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="LED4" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C4" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="LED5" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C5" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="P+6" library="supply1" deviceset="+5V" device=""/>
<part name="LED6" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C6" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="LED7" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C7" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="+5V" device=""/>
<part name="LED8" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C8" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="P+9" library="supply1" deviceset="+5V" device=""/>
<part name="LED9" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C9" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="P+10" library="supply1" deviceset="+5V" device=""/>
<part name="LED10" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C10" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="P+11" library="supply1" deviceset="+5V" device=""/>
<part name="LED11" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C11" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="P+12" library="supply1" deviceset="+5V" device=""/>
<part name="LED12" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C12" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="P+13" library="supply1" deviceset="+5V" device=""/>
<part name="LED13" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C13" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="P+14" library="supply1" deviceset="+5V" device=""/>
<part name="LED14" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C14" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="P+15" library="supply1" deviceset="+5V" device=""/>
<part name="LED15" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C15" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="P+16" library="supply1" deviceset="+5V" device=""/>
<part name="LED16" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C16" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="P+17" library="supply1" deviceset="+5V" device=""/>
<part name="LED17" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C17" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="P+18" library="supply1" deviceset="+5V" device=""/>
<part name="LED18" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C18" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="P+19" library="supply1" deviceset="+5V" device=""/>
<part name="LED19" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C19" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="P+20" library="supply1" deviceset="+5V" device=""/>
<part name="LED20" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C20" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="P+21" library="supply1" deviceset="+5V" device=""/>
<part name="LED21" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C21" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="P+22" library="supply1" deviceset="+5V" device=""/>
<part name="LED22" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C22" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="P+23" library="supply1" deviceset="+5V" device=""/>
<part name="LED23" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C23" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="P+24" library="supply1" deviceset="+5V" device=""/>
<part name="LED24" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C24" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
<part name="P+25" library="supply1" deviceset="+5V" device=""/>
<part name="LED25" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C25" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="P+26" library="supply1" deviceset="+5V" device=""/>
<part name="LED26" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C26" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="P+27" library="supply1" deviceset="+5V" device=""/>
<part name="LED27" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C27" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="P+28" library="supply1" deviceset="+5V" device=""/>
<part name="LED28" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C28" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="P+29" library="supply1" deviceset="+5V" device=""/>
<part name="LED29" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C29" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="P+30" library="supply1" deviceset="+5V" device=""/>
<part name="LED30" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C30" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="P+31" library="supply1" deviceset="+5V" device=""/>
<part name="LED31" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C31" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="P+32" library="supply1" deviceset="+5V" device=""/>
<part name="LED32" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C32" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="P+33" library="supply1" deviceset="+5V" device=""/>
<part name="LED33" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C33" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="GND67" library="supply1" deviceset="GND" device=""/>
<part name="P+34" library="supply1" deviceset="+5V" device=""/>
<part name="LED34" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C34" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND68" library="supply1" deviceset="GND" device=""/>
<part name="GND69" library="supply1" deviceset="GND" device=""/>
<part name="P+35" library="supply1" deviceset="+5V" device=""/>
<part name="LED35" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C35" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND70" library="supply1" deviceset="GND" device=""/>
<part name="P+36" library="supply1" deviceset="+5V" device=""/>
<part name="LED36" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C36" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND71" library="supply1" deviceset="GND" device=""/>
<part name="P+37" library="supply1" deviceset="+5V" device=""/>
<part name="LED37" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C37" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND72" library="supply1" deviceset="GND" device=""/>
<part name="P+38" library="supply1" deviceset="+5V" device=""/>
<part name="LED38" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C38" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND73" library="supply1" deviceset="GND" device=""/>
<part name="P+39" library="supply1" deviceset="+5V" device=""/>
<part name="LED39" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C39" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND74" library="supply1" deviceset="GND" device=""/>
<part name="P+40" library="supply1" deviceset="+5V" device=""/>
<part name="LED40" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C40" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND75" library="supply1" deviceset="GND" device=""/>
<part name="P+41" library="supply1" deviceset="+5V" device=""/>
<part name="LED41" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C41" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND76" library="supply1" deviceset="GND" device=""/>
<part name="GND77" library="supply1" deviceset="GND" device=""/>
<part name="P+42" library="supply1" deviceset="+5V" device=""/>
<part name="LED42" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C42" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND78" library="supply1" deviceset="GND" device=""/>
<part name="GND79" library="supply1" deviceset="GND" device=""/>
<part name="P+43" library="supply1" deviceset="+5V" device=""/>
<part name="LED43" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C43" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND80" library="supply1" deviceset="GND" device=""/>
<part name="GND81" library="supply1" deviceset="GND" device=""/>
<part name="P+44" library="supply1" deviceset="+5V" device=""/>
<part name="LED44" library="OSPIN" deviceset="WS2812B" device="_5050N" value="WS2812B_5050N"/>
<part name="C44" library="OSPIN" deviceset="CAPACITOR" device="0402" value="100n">
<attribute name="DIGIKEY_BEST_NR" value="587-1456-1-ND"/>
</part>
<part name="GND82" library="supply1" deviceset="GND" device=""/>
<part name="GND83" library="supply1" deviceset="GND" device=""/>
<part name="P+45" library="supply1" deviceset="+5V" device=""/>
<part name="GND84" library="supply1" deviceset="GND" device=""/>
<part name="GND85" library="supply1" deviceset="GND" device=""/>
<part name="GND86" library="supply1" deviceset="GND" device=""/>
<part name="GND87" library="supply1" deviceset="GND" device=""/>
<part name="GND88" library="supply1" deviceset="GND" device=""/>
<part name="GND89" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J1" gate="G$1" x="-40.64" y="35.56" rot="R90">
<attribute name="WE_BEST_NR" x="-40.64" y="35.56" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="P+1" gate="1" x="-27.94" y="22.86" rot="R180"/>
<instance part="GND1" gate="1" x="-27.94" y="43.18" rot="R180"/>
<instance part="LED1" gate="G$1" x="-2.54" y="35.56"/>
<instance part="C1" gate="G$1" x="7.62" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="7.62" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND2" gate="1" x="20.32" y="55.88" rot="R90"/>
<instance part="GND3" gate="1" x="-2.54" y="17.78"/>
<instance part="P+2" gate="1" x="2.54" y="63.5"/>
<instance part="LED2" gate="G$1" x="38.1" y="35.56"/>
<instance part="C2" gate="G$1" x="48.26" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="48.26" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND4" gate="1" x="60.96" y="55.88" rot="R90"/>
<instance part="GND5" gate="1" x="38.1" y="17.78"/>
<instance part="P+3" gate="1" x="43.18" y="63.5"/>
<instance part="LED3" gate="G$1" x="78.74" y="35.56"/>
<instance part="C3" gate="G$1" x="88.9" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="88.9" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND6" gate="1" x="101.6" y="55.88" rot="R90"/>
<instance part="GND7" gate="1" x="78.74" y="17.78"/>
<instance part="P+4" gate="1" x="83.82" y="63.5"/>
<instance part="LED4" gate="G$1" x="119.38" y="35.56"/>
<instance part="C4" gate="G$1" x="129.54" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="129.54" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND8" gate="1" x="142.24" y="55.88" rot="R90"/>
<instance part="GND9" gate="1" x="119.38" y="17.78"/>
<instance part="P+5" gate="1" x="124.46" y="63.5"/>
<instance part="LED5" gate="G$1" x="160.02" y="35.56"/>
<instance part="C5" gate="G$1" x="170.18" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="170.18" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND10" gate="1" x="182.88" y="55.88" rot="R90"/>
<instance part="GND11" gate="1" x="160.02" y="17.78"/>
<instance part="P+6" gate="1" x="165.1" y="63.5"/>
<instance part="LED6" gate="G$1" x="200.66" y="35.56"/>
<instance part="C6" gate="G$1" x="210.82" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="210.82" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND12" gate="1" x="223.52" y="55.88" rot="R90"/>
<instance part="GND13" gate="1" x="200.66" y="17.78"/>
<instance part="P+7" gate="1" x="205.74" y="63.5"/>
<instance part="LED7" gate="G$1" x="241.3" y="35.56"/>
<instance part="C7" gate="G$1" x="251.46" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="251.46" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND14" gate="1" x="264.16" y="55.88" rot="R90"/>
<instance part="GND15" gate="1" x="241.3" y="17.78"/>
<instance part="P+8" gate="1" x="246.38" y="63.5"/>
<instance part="LED8" gate="G$1" x="281.94" y="35.56"/>
<instance part="C8" gate="G$1" x="292.1" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="292.1" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND16" gate="1" x="304.8" y="55.88" rot="R90"/>
<instance part="GND17" gate="1" x="281.94" y="17.78"/>
<instance part="P+9" gate="1" x="287.02" y="63.5"/>
<instance part="LED9" gate="G$1" x="322.58" y="35.56"/>
<instance part="C9" gate="G$1" x="332.74" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="332.74" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND18" gate="1" x="345.44" y="55.88" rot="R90"/>
<instance part="GND19" gate="1" x="322.58" y="17.78"/>
<instance part="P+10" gate="1" x="327.66" y="63.5"/>
<instance part="LED10" gate="G$1" x="363.22" y="35.56"/>
<instance part="C10" gate="G$1" x="373.38" y="55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="373.38" y="55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND20" gate="1" x="386.08" y="55.88" rot="R90"/>
<instance part="GND21" gate="1" x="363.22" y="17.78"/>
<instance part="P+11" gate="1" x="368.3" y="63.5"/>
<instance part="LED11" gate="G$1" x="-2.54" y="-20.32"/>
<instance part="C11" gate="G$1" x="7.62" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="7.62" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND22" gate="1" x="20.32" y="0" rot="R90"/>
<instance part="GND23" gate="1" x="-2.54" y="-38.1"/>
<instance part="P+12" gate="1" x="2.54" y="7.62"/>
<instance part="LED12" gate="G$1" x="38.1" y="-20.32"/>
<instance part="C12" gate="G$1" x="48.26" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="48.26" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND24" gate="1" x="60.96" y="0" rot="R90"/>
<instance part="GND25" gate="1" x="38.1" y="-38.1"/>
<instance part="P+13" gate="1" x="43.18" y="7.62"/>
<instance part="LED13" gate="G$1" x="78.74" y="-20.32"/>
<instance part="C13" gate="G$1" x="88.9" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="88.9" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND26" gate="1" x="101.6" y="0" rot="R90"/>
<instance part="GND27" gate="1" x="78.74" y="-38.1"/>
<instance part="P+14" gate="1" x="83.82" y="7.62"/>
<instance part="LED14" gate="G$1" x="119.38" y="-20.32"/>
<instance part="C14" gate="G$1" x="129.54" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="129.54" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND28" gate="1" x="142.24" y="0" rot="R90"/>
<instance part="GND29" gate="1" x="119.38" y="-38.1"/>
<instance part="P+15" gate="1" x="124.46" y="7.62"/>
<instance part="LED15" gate="G$1" x="160.02" y="-20.32"/>
<instance part="C15" gate="G$1" x="170.18" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="170.18" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND30" gate="1" x="182.88" y="0" rot="R90"/>
<instance part="GND31" gate="1" x="160.02" y="-38.1"/>
<instance part="P+16" gate="1" x="165.1" y="7.62"/>
<instance part="LED16" gate="G$1" x="200.66" y="-20.32"/>
<instance part="C16" gate="G$1" x="210.82" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="210.82" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND32" gate="1" x="223.52" y="0" rot="R90"/>
<instance part="GND33" gate="1" x="200.66" y="-38.1"/>
<instance part="P+17" gate="1" x="205.74" y="7.62"/>
<instance part="LED17" gate="G$1" x="241.3" y="-20.32"/>
<instance part="C17" gate="G$1" x="251.46" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="251.46" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND34" gate="1" x="264.16" y="0" rot="R90"/>
<instance part="GND35" gate="1" x="241.3" y="-38.1"/>
<instance part="P+18" gate="1" x="246.38" y="7.62"/>
<instance part="LED18" gate="G$1" x="281.94" y="-20.32"/>
<instance part="C18" gate="G$1" x="292.1" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="292.1" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND36" gate="1" x="304.8" y="0" rot="R90"/>
<instance part="GND37" gate="1" x="281.94" y="-38.1"/>
<instance part="P+19" gate="1" x="287.02" y="7.62"/>
<instance part="LED19" gate="G$1" x="322.58" y="-20.32"/>
<instance part="C19" gate="G$1" x="332.74" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="332.74" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND38" gate="1" x="345.44" y="0" rot="R90"/>
<instance part="GND39" gate="1" x="322.58" y="-38.1"/>
<instance part="P+20" gate="1" x="327.66" y="7.62"/>
<instance part="LED20" gate="G$1" x="363.22" y="-20.32"/>
<instance part="C20" gate="G$1" x="373.38" y="0" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="373.38" y="0" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND40" gate="1" x="386.08" y="0" rot="R90"/>
<instance part="GND41" gate="1" x="363.22" y="-38.1"/>
<instance part="P+21" gate="1" x="368.3" y="7.62"/>
<instance part="LED21" gate="G$1" x="-2.54" y="-76.2"/>
<instance part="C21" gate="G$1" x="7.62" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="7.62" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND42" gate="1" x="20.32" y="-55.88" rot="R90"/>
<instance part="GND43" gate="1" x="-2.54" y="-93.98"/>
<instance part="P+22" gate="1" x="2.54" y="-48.26"/>
<instance part="LED22" gate="G$1" x="38.1" y="-76.2"/>
<instance part="C22" gate="G$1" x="48.26" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="48.26" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND44" gate="1" x="60.96" y="-55.88" rot="R90"/>
<instance part="GND45" gate="1" x="38.1" y="-93.98"/>
<instance part="P+23" gate="1" x="43.18" y="-48.26"/>
<instance part="LED23" gate="G$1" x="78.74" y="-76.2"/>
<instance part="C23" gate="G$1" x="88.9" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="88.9" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND46" gate="1" x="101.6" y="-55.88" rot="R90"/>
<instance part="GND47" gate="1" x="78.74" y="-93.98"/>
<instance part="P+24" gate="1" x="83.82" y="-48.26"/>
<instance part="LED24" gate="G$1" x="119.38" y="-76.2"/>
<instance part="C24" gate="G$1" x="129.54" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="129.54" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND48" gate="1" x="142.24" y="-55.88" rot="R90"/>
<instance part="GND49" gate="1" x="119.38" y="-93.98"/>
<instance part="P+25" gate="1" x="124.46" y="-48.26"/>
<instance part="LED25" gate="G$1" x="160.02" y="-76.2"/>
<instance part="C25" gate="G$1" x="170.18" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="170.18" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND50" gate="1" x="182.88" y="-55.88" rot="R90"/>
<instance part="GND51" gate="1" x="160.02" y="-93.98"/>
<instance part="P+26" gate="1" x="165.1" y="-48.26"/>
<instance part="LED26" gate="G$1" x="200.66" y="-76.2"/>
<instance part="C26" gate="G$1" x="210.82" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="210.82" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND52" gate="1" x="223.52" y="-55.88" rot="R90"/>
<instance part="GND53" gate="1" x="200.66" y="-93.98"/>
<instance part="P+27" gate="1" x="205.74" y="-48.26"/>
<instance part="LED27" gate="G$1" x="241.3" y="-76.2"/>
<instance part="C27" gate="G$1" x="251.46" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="251.46" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND54" gate="1" x="264.16" y="-55.88" rot="R90"/>
<instance part="GND55" gate="1" x="241.3" y="-93.98"/>
<instance part="P+28" gate="1" x="246.38" y="-48.26"/>
<instance part="LED28" gate="G$1" x="281.94" y="-76.2"/>
<instance part="C28" gate="G$1" x="292.1" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="292.1" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND56" gate="1" x="304.8" y="-55.88" rot="R90"/>
<instance part="GND57" gate="1" x="281.94" y="-93.98"/>
<instance part="P+29" gate="1" x="287.02" y="-48.26"/>
<instance part="LED29" gate="G$1" x="322.58" y="-76.2"/>
<instance part="C29" gate="G$1" x="332.74" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="332.74" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND58" gate="1" x="345.44" y="-55.88" rot="R90"/>
<instance part="GND59" gate="1" x="322.58" y="-93.98"/>
<instance part="P+30" gate="1" x="327.66" y="-48.26"/>
<instance part="LED30" gate="G$1" x="363.22" y="-76.2"/>
<instance part="C30" gate="G$1" x="373.38" y="-55.88" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="373.38" y="-55.88" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND60" gate="1" x="386.08" y="-55.88" rot="R90"/>
<instance part="GND61" gate="1" x="363.22" y="-93.98"/>
<instance part="P+31" gate="1" x="368.3" y="-48.26"/>
<instance part="LED31" gate="G$1" x="-2.54" y="-129.54"/>
<instance part="C31" gate="G$1" x="7.62" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="7.62" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND62" gate="1" x="20.32" y="-109.22" rot="R90"/>
<instance part="GND63" gate="1" x="-2.54" y="-147.32"/>
<instance part="P+32" gate="1" x="2.54" y="-101.6"/>
<instance part="LED32" gate="G$1" x="38.1" y="-129.54"/>
<instance part="C32" gate="G$1" x="48.26" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="48.26" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND64" gate="1" x="60.96" y="-109.22" rot="R90"/>
<instance part="GND65" gate="1" x="38.1" y="-147.32"/>
<instance part="P+33" gate="1" x="43.18" y="-101.6"/>
<instance part="LED33" gate="G$1" x="78.74" y="-129.54"/>
<instance part="C33" gate="G$1" x="88.9" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="88.9" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND66" gate="1" x="101.6" y="-109.22" rot="R90"/>
<instance part="GND67" gate="1" x="78.74" y="-147.32"/>
<instance part="P+34" gate="1" x="83.82" y="-101.6"/>
<instance part="LED34" gate="G$1" x="119.38" y="-129.54"/>
<instance part="C34" gate="G$1" x="129.54" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="129.54" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND68" gate="1" x="142.24" y="-109.22" rot="R90"/>
<instance part="GND69" gate="1" x="119.38" y="-147.32"/>
<instance part="P+35" gate="1" x="124.46" y="-101.6"/>
<instance part="LED35" gate="G$1" x="160.02" y="-129.54"/>
<instance part="C35" gate="G$1" x="170.18" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="170.18" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND70" gate="1" x="182.88" y="-109.22" rot="R90"/>
<instance part="P+36" gate="1" x="165.1" y="-101.6"/>
<instance part="LED36" gate="G$1" x="200.66" y="-129.54"/>
<instance part="C36" gate="G$1" x="210.82" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="210.82" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND71" gate="1" x="223.52" y="-109.22" rot="R90"/>
<instance part="P+37" gate="1" x="205.74" y="-101.6"/>
<instance part="LED37" gate="G$1" x="241.3" y="-129.54"/>
<instance part="C37" gate="G$1" x="251.46" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="251.46" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND72" gate="1" x="264.16" y="-109.22" rot="R90"/>
<instance part="P+38" gate="1" x="246.38" y="-101.6"/>
<instance part="LED38" gate="G$1" x="281.94" y="-129.54"/>
<instance part="C38" gate="G$1" x="292.1" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="292.1" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND73" gate="1" x="304.8" y="-109.22" rot="R90"/>
<instance part="P+39" gate="1" x="287.02" y="-101.6"/>
<instance part="LED39" gate="G$1" x="322.58" y="-129.54"/>
<instance part="C39" gate="G$1" x="332.74" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="332.74" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND74" gate="1" x="345.44" y="-109.22" rot="R90"/>
<instance part="P+40" gate="1" x="327.66" y="-101.6"/>
<instance part="LED40" gate="G$1" x="363.22" y="-129.54"/>
<instance part="C40" gate="G$1" x="373.38" y="-109.22" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="373.38" y="-109.22" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND75" gate="1" x="386.08" y="-109.22" rot="R90"/>
<instance part="P+41" gate="1" x="368.3" y="-101.6"/>
<instance part="LED41" gate="G$1" x="-2.54" y="-182.88"/>
<instance part="C41" gate="G$1" x="7.62" y="-162.56" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="7.62" y="-162.56" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND76" gate="1" x="20.32" y="-162.56" rot="R90"/>
<instance part="GND77" gate="1" x="-2.54" y="-200.66"/>
<instance part="P+42" gate="1" x="2.54" y="-154.94"/>
<instance part="LED42" gate="G$1" x="38.1" y="-182.88"/>
<instance part="C42" gate="G$1" x="48.26" y="-162.56" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="48.26" y="-162.56" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND78" gate="1" x="60.96" y="-162.56" rot="R90"/>
<instance part="GND79" gate="1" x="38.1" y="-200.66"/>
<instance part="P+43" gate="1" x="43.18" y="-154.94"/>
<instance part="LED43" gate="G$1" x="78.74" y="-182.88"/>
<instance part="C43" gate="G$1" x="88.9" y="-162.56" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="88.9" y="-162.56" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND80" gate="1" x="101.6" y="-162.56" rot="R90"/>
<instance part="GND81" gate="1" x="78.74" y="-200.66"/>
<instance part="P+44" gate="1" x="83.82" y="-154.94"/>
<instance part="LED44" gate="G$1" x="119.38" y="-182.88"/>
<instance part="C44" gate="G$1" x="129.54" y="-162.56" rot="MR270">
<attribute name="DIGIKEY_BEST_NR" x="129.54" y="-162.56" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND82" gate="1" x="142.24" y="-162.56" rot="R90"/>
<instance part="GND83" gate="1" x="119.38" y="-200.66"/>
<instance part="P+45" gate="1" x="124.46" y="-154.94"/>
<instance part="GND84" gate="1" x="160.02" y="-147.32"/>
<instance part="GND85" gate="1" x="200.66" y="-147.32"/>
<instance part="GND86" gate="1" x="241.3" y="-147.32"/>
<instance part="GND87" gate="1" x="281.94" y="-147.32"/>
<instance part="GND88" gate="1" x="322.58" y="-147.32"/>
<instance part="GND89" gate="1" x="363.22" y="-147.32"/>
</instances>
<busses>
</busses>
<nets>
<net name="DIN" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="-33.02" y1="33.02" x2="-15.24" y2="33.02" width="0.1524" layer="91"/>
<label x="-27.94" y="33.02" size="1.778" layer="95"/>
<pinref part="LED1" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-33.02" y1="38.1" x2="-27.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="38.1" x2="-27.94" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="12.7" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-2.54" y1="25.4" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="53.34" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="38.1" y1="25.4" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="93.98" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="78.74" y1="25.4" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="134.62" y1="55.88" x2="139.7" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="119.38" y1="25.4" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="175.26" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED5" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="160.02" y1="25.4" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="215.9" y1="55.88" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="200.66" y1="25.4" x2="200.66" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="256.54" y1="55.88" x2="261.62" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED7" gate="G$1" pin="GND"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="241.3" y1="25.4" x2="241.3" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="297.18" y1="55.88" x2="302.26" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED8" gate="G$1" pin="GND"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="281.94" y1="25.4" x2="281.94" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="337.82" y1="55.88" x2="342.9" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED9" gate="G$1" pin="GND"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="322.58" y1="25.4" x2="322.58" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="378.46" y1="55.88" x2="383.54" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED10" gate="G$1" pin="GND"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="363.22" y1="25.4" x2="363.22" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="12.7" y1="0" x2="17.78" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED11" gate="G$1" pin="GND"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-30.48" x2="-2.54" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="53.34" y1="0" x2="58.42" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED12" gate="G$1" pin="GND"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="38.1" y1="-30.48" x2="38.1" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="93.98" y1="0" x2="99.06" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED13" gate="G$1" pin="GND"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="78.74" y1="-30.48" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="134.62" y1="0" x2="139.7" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED14" gate="G$1" pin="GND"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="119.38" y1="-30.48" x2="119.38" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="175.26" y1="0" x2="180.34" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED15" gate="G$1" pin="GND"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="160.02" y1="-30.48" x2="160.02" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="215.9" y1="0" x2="220.98" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED16" gate="G$1" pin="GND"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="200.66" y1="-30.48" x2="200.66" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="256.54" y1="0" x2="261.62" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED17" gate="G$1" pin="GND"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="241.3" y1="-30.48" x2="241.3" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="297.18" y1="0" x2="302.26" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED18" gate="G$1" pin="GND"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="281.94" y1="-30.48" x2="281.94" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="337.82" y1="0" x2="342.9" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED19" gate="G$1" pin="GND"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="322.58" y1="-30.48" x2="322.58" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="378.46" y1="0" x2="383.54" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED20" gate="G$1" pin="GND"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="363.22" y1="-30.48" x2="363.22" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="12.7" y1="-55.88" x2="17.78" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED21" gate="G$1" pin="GND"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-86.36" x2="-2.54" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="53.34" y1="-55.88" x2="58.42" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED22" gate="G$1" pin="GND"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="38.1" y1="-86.36" x2="38.1" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="93.98" y1="-55.88" x2="99.06" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED23" gate="G$1" pin="GND"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="78.74" y1="-86.36" x2="78.74" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="134.62" y1="-55.88" x2="139.7" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED24" gate="G$1" pin="GND"/>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="119.38" y1="-86.36" x2="119.38" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="175.26" y1="-55.88" x2="180.34" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED25" gate="G$1" pin="GND"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="160.02" y1="-86.36" x2="160.02" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="215.9" y1="-55.88" x2="220.98" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED26" gate="G$1" pin="GND"/>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="200.66" y1="-86.36" x2="200.66" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="256.54" y1="-55.88" x2="261.62" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED27" gate="G$1" pin="GND"/>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="241.3" y1="-86.36" x2="241.3" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="297.18" y1="-55.88" x2="302.26" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED28" gate="G$1" pin="GND"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="281.94" y1="-86.36" x2="281.94" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="337.82" y1="-55.88" x2="342.9" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED29" gate="G$1" pin="GND"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="322.58" y1="-86.36" x2="322.58" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="378.46" y1="-55.88" x2="383.54" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED30" gate="G$1" pin="GND"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="363.22" y1="-86.36" x2="363.22" y2="-91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="12.7" y1="-109.22" x2="17.78" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED31" gate="G$1" pin="GND"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-139.7" x2="-2.54" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="53.34" y1="-109.22" x2="58.42" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED32" gate="G$1" pin="GND"/>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="38.1" y1="-139.7" x2="38.1" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
<wire x1="93.98" y1="-109.22" x2="99.06" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED33" gate="G$1" pin="GND"/>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="78.74" y1="-139.7" x2="78.74" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="GND68" gate="1" pin="GND"/>
<wire x1="134.62" y1="-109.22" x2="139.7" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED34" gate="G$1" pin="GND"/>
<pinref part="GND69" gate="1" pin="GND"/>
<wire x1="119.38" y1="-139.7" x2="119.38" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND70" gate="1" pin="GND"/>
<wire x1="175.26" y1="-109.22" x2="180.34" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED35" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="-139.7" x2="160.02" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND84" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="GND71" gate="1" pin="GND"/>
<wire x1="215.9" y1="-109.22" x2="220.98" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED36" gate="G$1" pin="GND"/>
<wire x1="200.66" y1="-139.7" x2="200.66" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND85" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="GND72" gate="1" pin="GND"/>
<wire x1="256.54" y1="-109.22" x2="261.62" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED37" gate="G$1" pin="GND"/>
<wire x1="241.3" y1="-139.7" x2="241.3" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND86" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="2"/>
<pinref part="GND73" gate="1" pin="GND"/>
<wire x1="297.18" y1="-109.22" x2="302.26" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED38" gate="G$1" pin="GND"/>
<wire x1="281.94" y1="-139.7" x2="281.94" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND87" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="GND74" gate="1" pin="GND"/>
<wire x1="337.82" y1="-109.22" x2="342.9" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED39" gate="G$1" pin="GND"/>
<wire x1="322.58" y1="-139.7" x2="322.58" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND88" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="GND75" gate="1" pin="GND"/>
<wire x1="378.46" y1="-109.22" x2="383.54" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED40" gate="G$1" pin="GND"/>
<wire x1="363.22" y1="-139.7" x2="363.22" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="GND89" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="GND76" gate="1" pin="GND"/>
<wire x1="12.7" y1="-162.56" x2="17.78" y2="-162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED41" gate="G$1" pin="GND"/>
<pinref part="GND77" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-193.04" x2="-2.54" y2="-198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="53.34" y1="-162.56" x2="58.42" y2="-162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED42" gate="G$1" pin="GND"/>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="38.1" y1="-193.04" x2="38.1" y2="-198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="93.98" y1="-162.56" x2="99.06" y2="-162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED43" gate="G$1" pin="GND"/>
<pinref part="GND81" gate="1" pin="GND"/>
<wire x1="78.74" y1="-193.04" x2="78.74" y2="-198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="GND82" gate="1" pin="GND"/>
<wire x1="134.62" y1="-162.56" x2="139.7" y2="-162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED44" gate="G$1" pin="GND"/>
<pinref part="GND83" gate="1" pin="GND"/>
<wire x1="119.38" y1="-193.04" x2="119.38" y2="-198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="-33.02" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="27.94" x2="-27.94" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="LED1" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="60.96" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="2.54" y1="55.88" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="5.08" y1="55.88" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<junction x="2.54" y="55.88"/>
</segment>
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="LED2" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="60.96" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="43.18" y1="55.88" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<junction x="43.18" y="55.88"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="LED3" gate="G$1" pin="VDD"/>
<wire x1="83.82" y1="60.96" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="55.88" x2="83.82" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<junction x="83.82" y="55.88"/>
</segment>
<segment>
<pinref part="P+5" gate="1" pin="+5V"/>
<pinref part="LED4" gate="G$1" pin="VDD"/>
<wire x1="124.46" y1="60.96" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="124.46" y1="55.88" x2="124.46" y2="50.8" width="0.1524" layer="91"/>
<wire x1="127" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<junction x="124.46" y="55.88"/>
</segment>
<segment>
<pinref part="P+6" gate="1" pin="+5V"/>
<pinref part="LED5" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="50.8" width="0.1524" layer="91"/>
<wire x1="167.64" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<junction x="165.1" y="55.88"/>
</segment>
<segment>
<pinref part="P+7" gate="1" pin="+5V"/>
<pinref part="LED6" gate="G$1" pin="VDD"/>
<wire x1="205.74" y1="60.96" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="205.74" y1="55.88" x2="205.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="208.28" y1="55.88" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
<junction x="205.74" y="55.88"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+5V"/>
<pinref part="LED7" gate="G$1" pin="VDD"/>
<wire x1="246.38" y1="60.96" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="246.38" y1="55.88" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="55.88" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<junction x="246.38" y="55.88"/>
</segment>
<segment>
<pinref part="P+9" gate="1" pin="+5V"/>
<pinref part="LED8" gate="G$1" pin="VDD"/>
<wire x1="287.02" y1="60.96" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="287.02" y1="55.88" x2="287.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="289.56" y1="55.88" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<junction x="287.02" y="55.88"/>
</segment>
<segment>
<pinref part="P+10" gate="1" pin="+5V"/>
<pinref part="LED9" gate="G$1" pin="VDD"/>
<wire x1="327.66" y1="60.96" x2="327.66" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="327.66" y1="55.88" x2="327.66" y2="50.8" width="0.1524" layer="91"/>
<wire x1="330.2" y1="55.88" x2="327.66" y2="55.88" width="0.1524" layer="91"/>
<junction x="327.66" y="55.88"/>
</segment>
<segment>
<pinref part="P+11" gate="1" pin="+5V"/>
<pinref part="LED10" gate="G$1" pin="VDD"/>
<wire x1="368.3" y1="60.96" x2="368.3" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="368.3" y1="55.88" x2="368.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="370.84" y1="55.88" x2="368.3" y2="55.88" width="0.1524" layer="91"/>
<junction x="368.3" y="55.88"/>
</segment>
<segment>
<pinref part="P+12" gate="1" pin="+5V"/>
<pinref part="LED11" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="0" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="91"/>
<junction x="2.54" y="0"/>
</segment>
<segment>
<pinref part="P+13" gate="1" pin="+5V"/>
<pinref part="LED12" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="5.08" x2="43.18" y2="0" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="43.18" y1="0" x2="43.18" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="0" x2="43.18" y2="0" width="0.1524" layer="91"/>
<junction x="43.18" y="0"/>
</segment>
<segment>
<pinref part="P+14" gate="1" pin="+5V"/>
<pinref part="LED13" gate="G$1" pin="VDD"/>
<wire x1="83.82" y1="5.08" x2="83.82" y2="0" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="83.82" y1="0" x2="83.82" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="0" x2="83.82" y2="0" width="0.1524" layer="91"/>
<junction x="83.82" y="0"/>
</segment>
<segment>
<pinref part="P+15" gate="1" pin="+5V"/>
<pinref part="LED14" gate="G$1" pin="VDD"/>
<wire x1="124.46" y1="5.08" x2="124.46" y2="0" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="124.46" y1="0" x2="124.46" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="127" y1="0" x2="124.46" y2="0" width="0.1524" layer="91"/>
<junction x="124.46" y="0"/>
</segment>
<segment>
<pinref part="P+16" gate="1" pin="+5V"/>
<pinref part="LED15" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="5.08" x2="165.1" y2="0" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="165.1" y1="0" x2="165.1" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="167.64" y1="0" x2="165.1" y2="0" width="0.1524" layer="91"/>
<junction x="165.1" y="0"/>
</segment>
<segment>
<pinref part="P+17" gate="1" pin="+5V"/>
<pinref part="LED16" gate="G$1" pin="VDD"/>
<wire x1="205.74" y1="5.08" x2="205.74" y2="0" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="205.74" y1="0" x2="205.74" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="208.28" y1="0" x2="205.74" y2="0" width="0.1524" layer="91"/>
<junction x="205.74" y="0"/>
</segment>
<segment>
<pinref part="P+18" gate="1" pin="+5V"/>
<pinref part="LED17" gate="G$1" pin="VDD"/>
<wire x1="246.38" y1="5.08" x2="246.38" y2="0" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="246.38" y1="0" x2="246.38" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="248.92" y1="0" x2="246.38" y2="0" width="0.1524" layer="91"/>
<junction x="246.38" y="0"/>
</segment>
<segment>
<pinref part="P+19" gate="1" pin="+5V"/>
<pinref part="LED18" gate="G$1" pin="VDD"/>
<wire x1="287.02" y1="5.08" x2="287.02" y2="0" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="287.02" y1="0" x2="287.02" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="289.56" y1="0" x2="287.02" y2="0" width="0.1524" layer="91"/>
<junction x="287.02" y="0"/>
</segment>
<segment>
<pinref part="P+20" gate="1" pin="+5V"/>
<pinref part="LED19" gate="G$1" pin="VDD"/>
<wire x1="327.66" y1="5.08" x2="327.66" y2="0" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="327.66" y1="0" x2="327.66" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="330.2" y1="0" x2="327.66" y2="0" width="0.1524" layer="91"/>
<junction x="327.66" y="0"/>
</segment>
<segment>
<pinref part="P+21" gate="1" pin="+5V"/>
<pinref part="LED20" gate="G$1" pin="VDD"/>
<wire x1="368.3" y1="5.08" x2="368.3" y2="0" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="368.3" y1="0" x2="368.3" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="370.84" y1="0" x2="368.3" y2="0" width="0.1524" layer="91"/>
<junction x="368.3" y="0"/>
</segment>
<segment>
<pinref part="P+22" gate="1" pin="+5V"/>
<pinref part="LED21" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="-50.8" x2="2.54" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="2.54" y1="-55.88" x2="2.54" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-55.88" x2="2.54" y2="-55.88" width="0.1524" layer="91"/>
<junction x="2.54" y="-55.88"/>
</segment>
<segment>
<pinref part="P+23" gate="1" pin="+5V"/>
<pinref part="LED22" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="-50.8" x2="43.18" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-55.88" x2="43.18" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-55.88" x2="43.18" y2="-55.88" width="0.1524" layer="91"/>
<junction x="43.18" y="-55.88"/>
</segment>
<segment>
<pinref part="P+24" gate="1" pin="+5V"/>
<pinref part="LED23" gate="G$1" pin="VDD"/>
<wire x1="83.82" y1="-50.8" x2="83.82" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-55.88" x2="83.82" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-55.88" x2="83.82" y2="-55.88" width="0.1524" layer="91"/>
<junction x="83.82" y="-55.88"/>
</segment>
<segment>
<pinref part="P+25" gate="1" pin="+5V"/>
<pinref part="LED24" gate="G$1" pin="VDD"/>
<wire x1="124.46" y1="-50.8" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="124.46" y1="-55.88" x2="124.46" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="127" y1="-55.88" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<junction x="124.46" y="-55.88"/>
</segment>
<segment>
<pinref part="P+26" gate="1" pin="+5V"/>
<pinref part="LED25" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="-50.8" x2="165.1" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-55.88" x2="165.1" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-55.88" x2="165.1" y2="-55.88" width="0.1524" layer="91"/>
<junction x="165.1" y="-55.88"/>
</segment>
<segment>
<pinref part="P+27" gate="1" pin="+5V"/>
<pinref part="LED26" gate="G$1" pin="VDD"/>
<wire x1="205.74" y1="-50.8" x2="205.74" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="205.74" y1="-55.88" x2="205.74" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-55.88" x2="205.74" y2="-55.88" width="0.1524" layer="91"/>
<junction x="205.74" y="-55.88"/>
</segment>
<segment>
<pinref part="P+28" gate="1" pin="+5V"/>
<pinref part="LED27" gate="G$1" pin="VDD"/>
<wire x1="246.38" y1="-50.8" x2="246.38" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-55.88" x2="246.38" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-55.88" x2="246.38" y2="-55.88" width="0.1524" layer="91"/>
<junction x="246.38" y="-55.88"/>
</segment>
<segment>
<pinref part="P+29" gate="1" pin="+5V"/>
<pinref part="LED28" gate="G$1" pin="VDD"/>
<wire x1="287.02" y1="-50.8" x2="287.02" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="287.02" y1="-55.88" x2="287.02" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="289.56" y1="-55.88" x2="287.02" y2="-55.88" width="0.1524" layer="91"/>
<junction x="287.02" y="-55.88"/>
</segment>
<segment>
<pinref part="P+30" gate="1" pin="+5V"/>
<pinref part="LED29" gate="G$1" pin="VDD"/>
<wire x1="327.66" y1="-50.8" x2="327.66" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="327.66" y1="-55.88" x2="327.66" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="330.2" y1="-55.88" x2="327.66" y2="-55.88" width="0.1524" layer="91"/>
<junction x="327.66" y="-55.88"/>
</segment>
<segment>
<pinref part="P+31" gate="1" pin="+5V"/>
<pinref part="LED30" gate="G$1" pin="VDD"/>
<wire x1="368.3" y1="-50.8" x2="368.3" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="368.3" y1="-55.88" x2="368.3" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="370.84" y1="-55.88" x2="368.3" y2="-55.88" width="0.1524" layer="91"/>
<junction x="368.3" y="-55.88"/>
</segment>
<segment>
<pinref part="P+32" gate="1" pin="+5V"/>
<pinref part="LED31" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="-104.14" x2="2.54" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="2.54" y1="-109.22" x2="2.54" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-109.22" x2="2.54" y2="-109.22" width="0.1524" layer="91"/>
<junction x="2.54" y="-109.22"/>
</segment>
<segment>
<pinref part="P+33" gate="1" pin="+5V"/>
<pinref part="LED32" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="-104.14" x2="43.18" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-109.22" x2="43.18" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-109.22" x2="43.18" y2="-109.22" width="0.1524" layer="91"/>
<junction x="43.18" y="-109.22"/>
</segment>
<segment>
<pinref part="P+34" gate="1" pin="+5V"/>
<pinref part="LED33" gate="G$1" pin="VDD"/>
<wire x1="83.82" y1="-104.14" x2="83.82" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-109.22" x2="83.82" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-109.22" x2="83.82" y2="-109.22" width="0.1524" layer="91"/>
<junction x="83.82" y="-109.22"/>
</segment>
<segment>
<pinref part="P+35" gate="1" pin="+5V"/>
<pinref part="LED34" gate="G$1" pin="VDD"/>
<wire x1="124.46" y1="-104.14" x2="124.46" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="124.46" y1="-109.22" x2="124.46" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="127" y1="-109.22" x2="124.46" y2="-109.22" width="0.1524" layer="91"/>
<junction x="124.46" y="-109.22"/>
</segment>
<segment>
<pinref part="P+36" gate="1" pin="+5V"/>
<pinref part="LED35" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="-104.14" x2="165.1" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-109.22" x2="165.1" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-109.22" x2="165.1" y2="-109.22" width="0.1524" layer="91"/>
<junction x="165.1" y="-109.22"/>
</segment>
<segment>
<pinref part="P+37" gate="1" pin="+5V"/>
<pinref part="LED36" gate="G$1" pin="VDD"/>
<wire x1="205.74" y1="-104.14" x2="205.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="205.74" y1="-109.22" x2="205.74" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-109.22" x2="205.74" y2="-109.22" width="0.1524" layer="91"/>
<junction x="205.74" y="-109.22"/>
</segment>
<segment>
<pinref part="P+38" gate="1" pin="+5V"/>
<pinref part="LED37" gate="G$1" pin="VDD"/>
<wire x1="246.38" y1="-104.14" x2="246.38" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-109.22" x2="246.38" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-109.22" x2="246.38" y2="-109.22" width="0.1524" layer="91"/>
<junction x="246.38" y="-109.22"/>
</segment>
<segment>
<pinref part="P+39" gate="1" pin="+5V"/>
<pinref part="LED38" gate="G$1" pin="VDD"/>
<wire x1="287.02" y1="-104.14" x2="287.02" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="287.02" y1="-109.22" x2="287.02" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="289.56" y1="-109.22" x2="287.02" y2="-109.22" width="0.1524" layer="91"/>
<junction x="287.02" y="-109.22"/>
</segment>
<segment>
<pinref part="P+40" gate="1" pin="+5V"/>
<pinref part="LED39" gate="G$1" pin="VDD"/>
<wire x1="327.66" y1="-104.14" x2="327.66" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="327.66" y1="-109.22" x2="327.66" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="330.2" y1="-109.22" x2="327.66" y2="-109.22" width="0.1524" layer="91"/>
<junction x="327.66" y="-109.22"/>
</segment>
<segment>
<pinref part="P+41" gate="1" pin="+5V"/>
<pinref part="LED40" gate="G$1" pin="VDD"/>
<wire x1="368.3" y1="-104.14" x2="368.3" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="368.3" y1="-109.22" x2="368.3" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="370.84" y1="-109.22" x2="368.3" y2="-109.22" width="0.1524" layer="91"/>
<junction x="368.3" y="-109.22"/>
</segment>
<segment>
<pinref part="P+42" gate="1" pin="+5V"/>
<pinref part="LED41" gate="G$1" pin="VDD"/>
<wire x1="2.54" y1="-157.48" x2="2.54" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="2.54" y1="-162.56" x2="2.54" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-162.56" x2="2.54" y2="-162.56" width="0.1524" layer="91"/>
<junction x="2.54" y="-162.56"/>
</segment>
<segment>
<pinref part="P+43" gate="1" pin="+5V"/>
<pinref part="LED42" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="-157.48" x2="43.18" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-162.56" x2="43.18" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-162.56" x2="43.18" y2="-162.56" width="0.1524" layer="91"/>
<junction x="43.18" y="-162.56"/>
</segment>
<segment>
<pinref part="P+44" gate="1" pin="+5V"/>
<pinref part="LED43" gate="G$1" pin="VDD"/>
<wire x1="83.82" y1="-157.48" x2="83.82" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-162.56" x2="83.82" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-162.56" x2="83.82" y2="-162.56" width="0.1524" layer="91"/>
<junction x="83.82" y="-162.56"/>
</segment>
<segment>
<pinref part="P+45" gate="1" pin="+5V"/>
<pinref part="LED44" gate="G$1" pin="VDD"/>
<wire x1="124.46" y1="-157.48" x2="124.46" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="124.46" y1="-162.56" x2="124.46" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="127" y1="-162.56" x2="124.46" y2="-162.56" width="0.1524" layer="91"/>
<junction x="124.46" y="-162.56"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="DO"/>
<pinref part="LED2" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="DO"/>
<pinref part="LED3" gate="G$1" pin="DI"/>
<wire x1="50.8" y1="33.02" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="DO"/>
<pinref part="LED4" gate="G$1" pin="DI"/>
<wire x1="91.44" y1="33.02" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="DO"/>
<wire x1="132.08" y1="33.02" x2="147.32" y2="33.02" width="0.1524" layer="91"/>
<pinref part="LED5" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LED5" gate="G$1" pin="DO"/>
<pinref part="LED6" gate="G$1" pin="DI"/>
<wire x1="172.72" y1="33.02" x2="187.96" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="DO"/>
<pinref part="LED7" gate="G$1" pin="DI"/>
<wire x1="213.36" y1="33.02" x2="228.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED7" gate="G$1" pin="DO"/>
<pinref part="LED8" gate="G$1" pin="DI"/>
<wire x1="254" y1="33.02" x2="269.24" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED8" gate="G$1" pin="DO"/>
<pinref part="LED9" gate="G$1" pin="DI"/>
<wire x1="294.64" y1="33.02" x2="309.88" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED9" gate="G$1" pin="DO"/>
<pinref part="LED10" gate="G$1" pin="DI"/>
<wire x1="335.28" y1="33.02" x2="350.52" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="LED10" gate="G$1" pin="DO"/>
<wire x1="375.92" y1="33.02" x2="381" y2="33.02" width="0.1524" layer="91"/>
<wire x1="381" y1="33.02" x2="381" y2="12.7" width="0.1524" layer="91"/>
<wire x1="381" y1="12.7" x2="-22.86" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="12.7" x2="-22.86" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="LED11" gate="G$1" pin="DI"/>
<wire x1="-22.86" y1="-22.86" x2="-15.24" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LED11" gate="G$1" pin="DO"/>
<pinref part="LED12" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="-22.86" x2="25.4" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="LED12" gate="G$1" pin="DO"/>
<pinref part="LED13" gate="G$1" pin="DI"/>
<wire x1="50.8" y1="-22.86" x2="66.04" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="LED13" gate="G$1" pin="DO"/>
<pinref part="LED14" gate="G$1" pin="DI"/>
<wire x1="91.44" y1="-22.86" x2="106.68" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LED14" gate="G$1" pin="DO"/>
<pinref part="LED15" gate="G$1" pin="DI"/>
<wire x1="132.08" y1="-22.86" x2="147.32" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="LED15" gate="G$1" pin="DO"/>
<pinref part="LED16" gate="G$1" pin="DI"/>
<wire x1="172.72" y1="-22.86" x2="187.96" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="LED16" gate="G$1" pin="DO"/>
<pinref part="LED17" gate="G$1" pin="DI"/>
<wire x1="213.36" y1="-22.86" x2="228.6" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="LED17" gate="G$1" pin="DO"/>
<pinref part="LED18" gate="G$1" pin="DI"/>
<wire x1="254" y1="-22.86" x2="269.24" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="LED18" gate="G$1" pin="DO"/>
<pinref part="LED19" gate="G$1" pin="DI"/>
<wire x1="294.64" y1="-22.86" x2="309.88" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="LED19" gate="G$1" pin="DO"/>
<pinref part="LED20" gate="G$1" pin="DI"/>
<wire x1="335.28" y1="-22.86" x2="350.52" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="LED20" gate="G$1" pin="DO"/>
<wire x1="375.92" y1="-22.86" x2="388.62" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-22.86" x2="388.62" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-43.18" x2="-22.86" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-43.18" x2="-22.86" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="LED21" gate="G$1" pin="DI"/>
<wire x1="-22.86" y1="-78.74" x2="-15.24" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="LED21" gate="G$1" pin="DO"/>
<pinref part="LED22" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="-78.74" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="LED22" gate="G$1" pin="DO"/>
<pinref part="LED23" gate="G$1" pin="DI"/>
<wire x1="50.8" y1="-78.74" x2="66.04" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="LED23" gate="G$1" pin="DO"/>
<pinref part="LED24" gate="G$1" pin="DI"/>
<wire x1="91.44" y1="-78.74" x2="106.68" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="LED24" gate="G$1" pin="DO"/>
<pinref part="LED25" gate="G$1" pin="DI"/>
<wire x1="132.08" y1="-78.74" x2="147.32" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="LED25" gate="G$1" pin="DO"/>
<wire x1="172.72" y1="-78.74" x2="187.96" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="LED26" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="LED27" gate="G$1" pin="DO"/>
<pinref part="LED28" gate="G$1" pin="DI"/>
<wire x1="254" y1="-78.74" x2="269.24" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="LED28" gate="G$1" pin="DO"/>
<wire x1="294.64" y1="-78.74" x2="309.88" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="LED29" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="LED29" gate="G$1" pin="DO"/>
<pinref part="LED30" gate="G$1" pin="DI"/>
<wire x1="335.28" y1="-78.74" x2="350.52" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="LED30" gate="G$1" pin="DO"/>
<wire x1="375.92" y1="-78.74" x2="388.62" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-78.74" x2="388.62" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-99.06" x2="-22.86" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-99.06" x2="-22.86" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="LED31" gate="G$1" pin="DI"/>
<wire x1="-22.86" y1="-132.08" x2="-15.24" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="LED31" gate="G$1" pin="DO"/>
<pinref part="LED32" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="-132.08" x2="25.4" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="LED32" gate="G$1" pin="DO"/>
<pinref part="LED33" gate="G$1" pin="DI"/>
<wire x1="50.8" y1="-132.08" x2="66.04" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="LED33" gate="G$1" pin="DO"/>
<pinref part="LED34" gate="G$1" pin="DI"/>
<wire x1="91.44" y1="-132.08" x2="106.68" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="LED26" gate="G$1" pin="DO"/>
<pinref part="LED27" gate="G$1" pin="DI"/>
<wire x1="213.36" y1="-78.74" x2="228.6" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="LED35" gate="G$1" pin="DO"/>
<wire x1="172.72" y1="-132.08" x2="187.96" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="LED36" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="LED37" gate="G$1" pin="DO"/>
<pinref part="LED38" gate="G$1" pin="DI"/>
<wire x1="254" y1="-132.08" x2="269.24" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="LED38" gate="G$1" pin="DO"/>
<wire x1="294.64" y1="-132.08" x2="309.88" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="LED39" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LED39" gate="G$1" pin="DO"/>
<pinref part="LED40" gate="G$1" pin="DI"/>
<wire x1="335.28" y1="-132.08" x2="350.52" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="LED40" gate="G$1" pin="DO"/>
<wire x1="375.92" y1="-132.08" x2="388.62" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-132.08" x2="388.62" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-152.4" x2="-22.86" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-152.4" x2="-22.86" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="LED41" gate="G$1" pin="DI"/>
<wire x1="-22.86" y1="-185.42" x2="-15.24" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="LED36" gate="G$1" pin="DO"/>
<pinref part="LED37" gate="G$1" pin="DI"/>
<wire x1="213.36" y1="-132.08" x2="228.6" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="LED41" gate="G$1" pin="DO"/>
<pinref part="LED42" gate="G$1" pin="DI"/>
<wire x1="10.16" y1="-185.42" x2="25.4" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LED42" gate="G$1" pin="DO"/>
<pinref part="LED43" gate="G$1" pin="DI"/>
<wire x1="50.8" y1="-185.42" x2="66.04" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="LED43" gate="G$1" pin="DO"/>
<pinref part="LED44" gate="G$1" pin="DI"/>
<wire x1="91.44" y1="-185.42" x2="106.68" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="LED34" gate="G$1" pin="DO"/>
<pinref part="LED35" gate="G$1" pin="DI"/>
<wire x1="132.08" y1="-132.08" x2="147.32" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,2.54,50.8,LED1,VDD,+5V,,,"/>
<approved hash="104,1,43.18,50.8,LED2,VDD,+5V,,,"/>
<approved hash="104,1,83.82,50.8,LED3,VDD,+5V,,,"/>
<approved hash="104,1,124.46,50.8,LED4,VDD,+5V,,,"/>
<approved hash="104,1,165.1,50.8,LED5,VDD,+5V,,,"/>
<approved hash="104,1,205.74,50.8,LED6,VDD,+5V,,,"/>
<approved hash="104,1,246.38,50.8,LED7,VDD,+5V,,,"/>
<approved hash="104,1,287.02,50.8,LED8,VDD,+5V,,,"/>
<approved hash="104,1,327.66,50.8,LED9,VDD,+5V,,,"/>
<approved hash="104,1,368.3,50.8,LED10,VDD,+5V,,,"/>
<approved hash="104,1,2.54,-5.08,LED11,VDD,+5V,,,"/>
<approved hash="104,1,43.18,-5.08,LED12,VDD,+5V,,,"/>
<approved hash="104,1,83.82,-5.08,LED13,VDD,+5V,,,"/>
<approved hash="104,1,124.46,-5.08,LED14,VDD,+5V,,,"/>
<approved hash="104,1,165.1,-5.08,LED15,VDD,+5V,,,"/>
<approved hash="104,1,205.74,-5.08,LED16,VDD,+5V,,,"/>
<approved hash="104,1,246.38,-5.08,LED17,VDD,+5V,,,"/>
<approved hash="104,1,287.02,-5.08,LED18,VDD,+5V,,,"/>
<approved hash="104,1,327.66,-5.08,LED19,VDD,+5V,,,"/>
<approved hash="104,1,368.3,-5.08,LED20,VDD,+5V,,,"/>
<approved hash="104,1,2.54,-60.96,LED21,VDD,+5V,,,"/>
<approved hash="104,1,43.18,-60.96,LED22,VDD,+5V,,,"/>
<approved hash="104,1,83.82,-60.96,LED23,VDD,+5V,,,"/>
<approved hash="104,1,124.46,-60.96,LED24,VDD,+5V,,,"/>
<approved hash="104,1,165.1,-60.96,LED25,VDD,+5V,,,"/>
<approved hash="104,1,205.74,-60.96,LED26,VDD,+5V,,,"/>
<approved hash="104,1,246.38,-60.96,LED27,VDD,+5V,,,"/>
<approved hash="104,1,287.02,-60.96,LED28,VDD,+5V,,,"/>
<approved hash="104,1,327.66,-60.96,LED29,VDD,+5V,,,"/>
<approved hash="104,1,368.3,-60.96,LED30,VDD,+5V,,,"/>
<approved hash="104,1,2.54,-114.3,LED31,VDD,+5V,,,"/>
<approved hash="104,1,43.18,-114.3,LED32,VDD,+5V,,,"/>
<approved hash="104,1,83.82,-114.3,LED33,VDD,+5V,,,"/>
<approved hash="104,1,124.46,-114.3,LED34,VDD,+5V,,,"/>
<approved hash="104,1,165.1,-114.3,LED35,VDD,+5V,,,"/>
<approved hash="104,1,205.74,-114.3,LED36,VDD,+5V,,,"/>
<approved hash="104,1,246.38,-114.3,LED37,VDD,+5V,,,"/>
<approved hash="104,1,287.02,-114.3,LED38,VDD,+5V,,,"/>
<approved hash="104,1,327.66,-114.3,LED39,VDD,+5V,,,"/>
<approved hash="104,1,368.3,-114.3,LED40,VDD,+5V,,,"/>
<approved hash="104,1,2.54,-167.64,LED41,VDD,+5V,,,"/>
<approved hash="104,1,43.18,-167.64,LED42,VDD,+5V,,,"/>
<approved hash="104,1,83.82,-167.64,LED43,VDD,+5V,,,"/>
<approved hash="104,1,124.46,-167.64,LED44,VDD,+5V,,,"/>
<approved hash="113,1,-36.0045,35.3129,J1,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
